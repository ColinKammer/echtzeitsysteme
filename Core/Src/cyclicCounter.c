#include "cyclicCounter.h"

#include "main.h"

#include "timer.h"

void cyclicCounter_tick();

KERNEL_SOFTTIMER(cyclicCounterTmr,"CyclicCounterTask",2000,0x400,1234,cyclicCounter_tick);

KERNEL_CREATE_MESSAGEQUEUE(cyclicCounter_mq,128,sizeof(uint32_t));

uint32_t counter = 12345678;

void cyclicCounter_tick()
{
	if(messageQueue_trySend(&cyclicCounter_mq, &counter, 0) == MessageQueueOK)
	{
		counter++; //only update if successfully placed
	}
}


void cyclicCounter_init()
{
	kernel_timer_setInterval(&cyclicCounterTmr, 5000); //5 second interval
	kernel_timer_start(&cyclicCounterTmr);
}
