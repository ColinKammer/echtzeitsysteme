#include "semaphore.h"

#include "SEGGER_RTT.h"

#include "kernel.h"
#include "task.h"
#include "block_timed.h"

Semaphore semaphoreTest_sem = KERNEL_NEWSEMAPHORE(2);

KERNEL_CREATETASK(semTest_MAS,"SemTest_MAS",1100,0x400,10);
KERNEL_CREATETASK(semTest_SL1,"SemTest_SL1",1100,0x400,11);
KERNEL_CREATETASK(semTest_SL2,"SemTest_SL2",1200,0x400,12);

void semTest_slave1Task()
{
	while(1)
	{
		semaphore_acquire(&semaphoreTest_sem);
		SEGGER_RTT_TerminalOut(0, "SemaphoreTest1\n");
	}
}
void semTest_slave2Task()
{
	while(1)
	{
		semaphore_acquire(&semaphoreTest_sem);
		SEGGER_RTT_TerminalOut(0, "SemaphoreTest2\n");
	}
}

void semaphoreTest_masterTask()
{
	while(1)
	{
		kernel_task_block_time(g_task_running,
				durationToEndTicks(7000));
		semaphore_release_allWaiting(&semaphoreTest_sem);
		kernel_task_block_time(g_task_running,
				durationToEndTicks(3000));
		semaphore_release_one(&semaphoreTest_sem);
	}
}

void semaphoreTest_init()
{
	kernel_startTask(&semTest_SL1, semTest_slave1Task, 0);
	kernel_startTask(&semTest_SL2, semTest_slave2Task, 0);
	kernel_startTask(&semTest_MAS, semaphoreTest_masterTask, 0);
}
