#pragma once

#pragma once
#include <stdlib.h>

#include "kernel.h"
#include "semaphore.h"

extern Semaphore semaphoreTest_sem;

void semaphoreTest_init();
