#include <stdint.h>
#include "SEGGER_SYSVIEW.h"
#include "SEGGER_SYSVIEW_Conf.h"

#ifndef SYSVIEW_CONFIG
#define SYSVIEW_CONFIG

// SystemcoreClock can be used in most CMSIS compatible projects.
// In non-CMSIS projects define SYSVIEW_CPU_FREQ.
//extern unsigned int SystemCoreClock;
/*********************************************************************
*
* Defines, configurable
*
**********************************************************************
*/
// The application name to be displayed in SystemViewer
#define SYSVIEW_APP_NAME "RtSystems App"

// The target device name
#define SYSVIEW_DEVICE_NAME "Cortex-M4"

// Frequency of the timestamp. Must match SEGGER_SYSVIEW_Conf.h
#define SYSVIEW_TIMESTAMP_FREQ (80000000)

// System Frequency. SystemcoreClock is used in most CMSIS compatible projects.
#define SYSVIEW_CPU_FREQ (80000000)

// The lowest RAM address used for IDs (pointers)
#define SYSVIEW_RAM_BASE (0x10000000)

// Define as 1 if the Cortex - M cycle counter is used as SystemView timestamp.Must match SEGGER_SYSVIEW_Conf.h
#ifndef USE_CYCCNT_TIMESTAMP
	#define USE_CYCCNT_TIMESTAMP 1
#endif
// Define as 1 if the Cortex - M cycle counter is used and there might be no debugger attached while recording.
#ifndef ENABLE_DWT_CYCCNT
	#define ENABLE_DWT_CYCCNT (USE_CYCCNT_TIMESTAMP &SEGGER_SYSVIEW_POST_MORTEM_MODE)
#endif
/*********************************************************************
*
* Defines, fixed
*
**********************************************************************
*/
#define DEMCR (*(volatile unsigned long *)(0xE000EDFCuL))
// Debug Exception and Monitor Control Register
#define TRACEENA_BIT (1uL << 24)
// Trace enable bit
#define DWT_CTRL (*(volatile unsigned long *)(0xE0001000uL))
// DWT Control Register
#define NOCYCCNT_BIT (1uL << 25)
// Cycle counter support bit
#define CYCCNTENA_BIT (1uL << 0)
// Cycle counter enable bit

void SEGGER_SYSVIEW_Conf(void);

#endif
