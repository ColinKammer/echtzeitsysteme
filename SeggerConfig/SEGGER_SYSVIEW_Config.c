#include "SEGGER_SYSVIEW_Config.h"

static void cbSendSystemDesc()
{
    SEGGER_SYSVIEW_SendSysDesc("N=" SYSVIEW_APP_NAME ",D=" SYSVIEW_DEVICE_NAME);
    SEGGER_SYSVIEW_SendSysDesc("I#15=SysTick");
}

void SEGGER_SYSVIEW_Conf(void)
{
//#ifdef USE_CYCCNT_TIMESTAMP
//#ifdef ENABLE_DWT_CYCCNT
//    //
//    // If no debugger is connected, the DWT must be enabled by the application
//    //
//    if ((DEMCR & TRACEENA_BIT) == 0)
//    {
//        DEMCR |= TRACEENA_BIT;
//    }
//#endif
//#endif

	SEGGER_SYSVIEW_Init(SYSVIEW_TIMESTAMP_FREQ, SYSVIEW_CPU_FREQ,
			0, cbSendSystemDesc);
	SEGGER_SYSVIEW_Start();


}
