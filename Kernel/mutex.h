#pragma once

#include "semaphore.h"
#include "kernel.h"

typedef struct {
	Semaphore m_sem;
	Task_TCB* m_lockOwner;
} Mutex;

#define KERNEL_NEWMUTEX() \
{\
	.m_sem = KERNEL_NEWSEMAPHORE(1),\
	.m_lockOwner = 0\
}

void mutex_lock(Mutex* mtx);
int mutex_tryLock(Mutex* mtx); //true if successful

void mutex_unlock(Mutex* mtx);
