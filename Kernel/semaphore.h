#pragma once

#include <stdint.h>
#include <stddef.h>

#include "task.h"

typedef struct {
	uint32_t m_cnt;
	uint32_t m_max;
	Task_TCB* m_waitingTasks;
} Semaphore;

#define KERNEL_NEWSEMAPHORE(MAXCOUNT) \
{\
	.m_cnt = MAXCOUNT,\
	.m_max = MAXCOUNT,\
	.m_waitingTasks = NULL\
}

void semaphore_acquire(Semaphore* sem);
int semaphore_tryAcquire(Semaphore* sem); //true if successful

void semaphore_release_one(Semaphore* sem);
void semaphore_release_allWaiting(Semaphore* sem);
