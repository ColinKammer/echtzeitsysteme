#include "block_customCb.h"

#include <assert.h>

#include "kernel.h"
#include "taskList.h"

#include "main.h"
#include "Segger/helper.h"

Task_TCB* g_tasks_blocked_byCustomCb = NULL;

void kernel_task_block_customCb(Task_TCB* tcb, CheckUnblockFunc funcCb, void* funcCbContext)
{
	assert(tcb == g_task_running);
	__disable_irq();
	SEGGER_SYSVIEW_OnTaskStopReady((unsigned)tcb, 'C');
	tcb->m_blockContext = funcCb;
	tcb->m_blockParam = funcCbContext;
	tcb->m_state = BlockedByCustomCb;
	taskList_pushFront(&g_tasks_blocked_byCustomCb, tcb);
	__enable_irq();
	kernel_yield();
}

void updateCustomCbTask(Task_TCB* tcb)
{
	CheckUnblockFunc func = tcb->m_blockContext;
	void* ctx = tcb->m_blockParam;
	if(func(ctx))
	{
		taskList_remove(&g_tasks_blocked_byCustomCb, tcb);
		kernel_task_ready(tcb);
	}
}

void kernel_task_block_customCb_updateAll()
{
	for(Task_TCB* task = g_tasks_blocked_byCustomCb; task != NULL; task = task->m_nextInList)
	{
		assert(task->m_state == BlockedByCustomCb);
		updateCustomCbTask(task);
	}
}
