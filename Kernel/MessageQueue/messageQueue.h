#pragma once

#include <stdint.h>

#include "task.h"

typedef struct {
	uint8_t* m_storage;
	int32_t m_readPos;			 //next index that will be read from, -1 if queue_empty
	int32_t m_writePos;          //next index that will be written to
	uint32_t m_maxNoElements;
	uint32_t m_ElementSize;
	Task_TCB* m_blockedReceive;
} MessageQueue_QCB;

#define KERNEL_CREATE_MESSAGEQUEUE(NAME, MAX_NO_ELEMENTS, ELEMENT_SIZE)\
	uint8_t NAME##_storage[MAX_NO_ELEMENTS*ELEMENT_SIZE];\
	MessageQueue_QCB NAME = {\
		.m_storage = NAME##_storage,\
		.m_readPos = -1,\
		.m_writePos = 0,\
		.m_maxNoElements = MAX_NO_ELEMENTS,\
		.m_ElementSize = ELEMENT_SIZE,\
		.m_blockedReceive = NULL\
	};

typedef enum {
	MessageQueueOK,
	MessageQueueFull,
	MessageQueueEmpty,

} MessageQueue_Result;

MessageQueue_Result messageQueue_trySend(MessageQueue_QCB* qcb, const void* element, uint8_t boBroadcast);

MessageQueue_Result messageQueue_tryReceive(MessageQueue_QCB* qcb, void* element_out);

void messageQueue_receive(MessageQueue_QCB* qcb, void* element_out);
