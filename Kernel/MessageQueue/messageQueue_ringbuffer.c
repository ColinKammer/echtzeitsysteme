#include "messageQueue.h"

#include <string.h>

#include "main.h"
#include "taskList.h"
#include "kernel.h"

void* mq_ElementAt(MessageQueue_QCB* qcb, int32_t index)
{
	return qcb->m_storage + index * qcb->m_ElementSize;
}


void ringbuffer_dequeue(MessageQueue_QCB* qcb, void* element_out)
{
	void* queueElement = mq_ElementAt(qcb,qcb->m_readPos);
	memcpy(element_out, queueElement, qcb->m_ElementSize);

	int32_t nextRdPos = qcb->m_readPos + 1;
	if(nextRdPos == qcb->m_maxNoElements)
		nextRdPos = 0; //loop around
	if(nextRdPos == qcb->m_writePos)
		nextRdPos = -1; //nothing written there yet
	qcb->m_readPos = nextRdPos;
}

void ringbuffer_enqueue(MessageQueue_QCB* qcb, const void* element_in)
{
	void* queueElement = mq_ElementAt(qcb, qcb->m_writePos);
	memcpy(queueElement, element_in, qcb->m_ElementSize);

	//update writeIndex
	int32_t nextWrPos = qcb->m_writePos + 1;
	if(nextWrPos == qcb->m_maxNoElements)
		nextWrPos = 0; //loop around
	qcb->m_writePos = nextWrPos;
}


MessageQueue_Result messageQueue_trySend(MessageQueue_QCB* qcb, const void* element, uint8_t boBroadcast)
{
	__disable_irq();

	if(qcb->m_writePos == qcb->m_readPos)
	{
		//can't overwrite element that is next to be read -> queue full
		__enable_irq();
		return MessageQueueFull;
	}

	if((qcb->m_readPos == -1) && (qcb->m_blockedReceive != NULL))
	{
		//queue was empty before and at least one task is waiting
		do
		{
			Task_TCB* tsk = qcb->m_blockedReceive; //maybe todo: get the one with highest priority
			memcpy(tsk->m_blockParam, element, qcb->m_ElementSize); //copy into the tasks buffer
			tsk->m_state = Ready;
			taskList_remove(&(qcb->m_blockedReceive), tsk);
			taskList_pushFront(&g_tasks_ready, tsk);
		}
		while(boBroadcast && (qcb->m_blockedReceive != NULL));
	}
	else if((qcb->m_readPos == -1) && (qcb->m_blockedReceive == NULL))
	{
		//queue empty, but nobody is waiting
		qcb->m_readPos = qcb->m_writePos; //the written element is the next to be read
		ringbuffer_enqueue(qcb, element);
	}
	else if(qcb->m_readPos >= 0)
	{
		//queue already contains at least one element
		assert(qcb->m_blockedReceive == NULL); //therefore there can not be any waiting tasks
		ringbuffer_enqueue(qcb, element);
	}
	else
	{
		assert(0); //one of the previous branches should match
	}
	__enable_irq();
	return MessageQueueOK;
}


MessageQueue_Result messageQueue_tryReceive(MessageQueue_QCB* qcb, void* element_out)
{
	__disable_irq();
	if(qcb->m_readPos < 0)
	{
		__enable_irq();
		return MessageQueueEmpty;
	}
	else
	{
		ringbuffer_dequeue(qcb, element_out);
		__enable_irq();
		return MessageQueueOK;
	}
}


void messageQueue_receive(MessageQueue_QCB* qcb, void* element_out)
{
	__disable_irq();
	if(qcb->m_readPos < 0)
	{
		//Queue empty -> block
		g_task_running->m_blockParam = element_out;
		taskList_pushFront(&(qcb->m_blockedReceive), g_task_running);
		g_task_running->m_state = BlockedByMessageQueue;
		__enable_irq();
		kernel_yield();
	}
	else
	{
		ringbuffer_dequeue(qcb, element_out);
		__enable_irq();
	}
}
