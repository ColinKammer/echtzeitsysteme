#pragma once

#include <stdint.h>

#include "task.h"

typedef int (*CheckUnblockFunc)(void* context);

void kernel_task_block_customCb(Task_TCB* tcb, CheckUnblockFunc funcCb, void* funcCbContext);

void kernel_task_block_customCb_updateAll();
