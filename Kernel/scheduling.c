#include "kernel.h"

#include <stdlib.h>
#include <assert.h>
#include "Segger/helper.h"
#include "main.h"

#include "block_customCb.h"
#include "block_timed.h"

#include "taskList.h"

void kernel_task_ready(Task_TCB* tcb)
{
	SEGGER_SYSVIEW_OnTaskStartReady((unsigned)tcb);
	taskList_pushFront(&g_tasks_ready, tcb);
	tcb->m_state = Ready;
}

void kernel_updateTasks()
{
	kernel_task_block_customCb_updateAll();
	kernel_task_block_time_updateAll();
}

Task_TCB* kernel_getNextTask()
{
	uint32_t highestPriorityYet = 0xFFFFFFFF;
	Task_TCB* taskWithHighestPriorityYet = NULL;

	if(g_task_running && (g_task_running->m_state == Running))
	{
		highestPriorityYet = g_task_running->m_priority;
		taskWithHighestPriorityYet = g_task_running;
	}

	for(Task_TCB* task = g_tasks_ready; task != NULL; task = task->m_nextInList)
	{
		if(task->m_priority < highestPriorityYet)
		{
			highestPriorityYet = task->m_priority;
			taskWithHighestPriorityYet = task;
		}
	}
	assert(taskWithHighestPriorityYet != NULL);
	return taskWithHighestPriorityYet;
}
