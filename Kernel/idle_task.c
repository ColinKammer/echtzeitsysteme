#include "idle_task.h"

#include <stdlib.h>

KERNEL_CREATETASK(g_idleTask, "idle", 1000000, 128, 0);
void idle_task_main()
{
	while (1)
	{
		//permanently try to release the cpu
		//kernel_yield();
		//or just rely on Systick
	}
}

