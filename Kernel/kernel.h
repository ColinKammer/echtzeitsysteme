#pragma once
#include <stdint.h>

#include "task.h"

extern Task_TCB* g_task_running;

extern Task_TCB* g_tasks_ready;

void kernel_startTask(Task_TCB* tcb, EntryPointFunc entryPoint, void* param1);

void kernel_yield();

void kernel_start();
extern volatile uint8_t g_kernel_isRunning;

void kernel_updateTasks();
void kernel_task_ready(Task_TCB* tcb);
Task_TCB* kernel_getNextTask();
void kernel_switchContextToTask(Task_TCB* nextTask);

void SV_Handler(void);

#define SAVE_REMAINING_REGISTERS_OF_CURRENT_TASK(FUNCNAME) \
		asm volatile(\
				".global g_task_running\n"\
				"LDR	R0,  =g_task_running\n"\
				"LDR	R0,  [R0]\n"\
				"CBZ	R0,	 jmpMark" FUNCNAME "\n"\
				"STR	R4,  [R0, #0 ]\n"\
				"STR	R5,  [R0, #4 ]\n"\
				"STR	R6,  [R0, #8 ]\n"\
				"STR	R7,  [R0, #12]\n"\
				"STR	R8,  [R0, #16]\n"\
				"STR	R9,  [R0, #20]\n"\
				"STR	R10, [R0, #24]\n"\
				"STR	R11, [R0, #28]\n"\
				"jmpMark" FUNCNAME ":"\
				"NOP"\
		)

#define RESTORE_REMAINING_REGISTERS_OF_CURRENT_TASK(FUNCNAME) \
		asm volatile(\
					".global g_task_running\n"\
					"LDR	R0,  =g_task_running\n"\
					"LDR	R0,  [R0]\n"\
					"LDR	R4,  [R0, #0 ]\n"\
					"LDR	R5,  [R0, #4 ]\n"\
					"LDR	R6,  [R0, #8 ]\n"\
					"LDR	R7,  [R0, #12]\n"\
					"LDR	R8,  [R0, #16]\n"\
					"LDR	R9,  [R0, #20]\n"\
					"LDR	R10, [R0, #24]\n"\
					"LDR	R11, [R0, #28]\n"\
		)
