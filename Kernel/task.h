#pragma once

#include <stdint.h>

typedef enum {
	Running,
	Ready,
	Finished,
	BlockedByCustomCb,
	BlockedByTime,
	BlockedBySemaphore,
	BlockedByMessageQueue
} Task_State;

typedef void (*EntryPointFunc)(void* param1);

typedef struct Task_TCB{
	uint32_t m_extendedRegisterSet[8];
	struct Task_TCB* m_nextInList;
	struct Task_TCB* m_prevInList;
	uint32_t m_id;
	uint32_t m_priority;
	uint8_t* m_stackBase;
	uint32_t* m_stackPtr;
	const char* m_name;
	void* m_blockContext;
	void* m_blockParam;
	Task_State m_state;
} Task_TCB;

#define KERNEL_CREATETASK(NAME, STRNAME, PRIORITY, STACKSIZE, ID)\
	uint8_t NAME##_stack[STACKSIZE];\
	Task_TCB NAME = {\
		.m_nextInList = NULL,\
		.m_prevInList = NULL,\
		.m_id = ID,\
		.m_name = STRNAME,\
		.m_priority = PRIORITY,\
		.m_stackBase = NAME##_stack,\
		.m_stackPtr = (uint32_t*)(NAME##_stack + STACKSIZE),\
		.m_blockContext = NULL,\
		.m_blockParam = NULL,\
		.m_state = Ready\
	};
