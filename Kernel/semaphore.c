#include "semaphore.h"

#include <assert.h>
#include <cmsis_gcc.h>

#include "taskList.h"
#include "kernel.h"

void semaphore_acquire(Semaphore* sem)
{
	//maybe todo Bugfix: save irq state instead of overwriting
	__disable_irq();
	if(sem->m_cnt > 0)
	{
		sem->m_cnt--;
		//Successfully acquired, no need to block
		__enable_irq();
		return;
	}
	//block
	taskList_pushFront(&(sem->m_waitingTasks), g_task_running);
	g_task_running->m_state = BlockedBySemaphore;
	__enable_irq();
	kernel_yield();
}

int semaphore_tryAcquire(Semaphore* sem)
{
	__disable_irq();
	if(sem->m_cnt > 0)
	{
		sem->m_cnt--;
		__enable_irq();
		return 1;
	}
	else
	{
		__enable_irq();
		return 0;
	}
}

void semaphore_release_one(Semaphore* sem)
{
	__disable_irq();
	if(sem->m_waitingTasks)
	{
		//release waiting tasks first
		Task_TCB* tsk = sem->m_waitingTasks; //maybe todo: get the one with highest priority
		tsk->m_state = Ready;
		taskList_remove(&(sem->m_waitingTasks), tsk);
		taskList_pushFront(&g_tasks_ready, tsk);
	}
	else
	{
		sem->m_cnt++;
		assert(sem->m_cnt <= sem->m_max);
	}
	__enable_irq();
}

void semaphore_release_allWaiting(Semaphore* sem)
{
	__disable_irq();
	while(sem->m_waitingTasks)
	{
		Task_TCB* tsk = sem->m_waitingTasks;
		tsk->m_state = Ready;
		taskList_remove(&(sem->m_waitingTasks), tsk); //new list head
		taskList_pushFront(&g_tasks_ready, tsk);
	}
	__enable_irq();
}
