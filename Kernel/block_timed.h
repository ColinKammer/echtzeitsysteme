#pragma once

#include <stdint.h>

#include "task.h"

void kernel_task_block_time(Task_TCB* tcb, uint32_t untilTicksEq);

void kernel_task_block_time_updateAll();

uint32_t durationToTicks(uint32_t durationMs);
uint32_t durationToEndTicks(uint32_t durationMs);
