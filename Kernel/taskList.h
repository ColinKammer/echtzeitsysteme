#pragma once

#include "task.h"

void taskList_pushFront(Task_TCB** list, Task_TCB* element);
void taskList_remove(Task_TCB** list, Task_TCB* element);
