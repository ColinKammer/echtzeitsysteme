#include "kernel.h"

#include <stdlib.h>
#include <assert.h>

#include "stm32l4xx_hal.h"
#include "Segger/helper.h"

#include "idle_task.h"
#include "taskList.h"

Task_TCB* g_task_running = NULL;

Task_TCB* g_tasks_ready = NULL;

uint32_t g_kernel_nextTaskId = 0;
volatile uint8_t g_kernel_isRunning = 0;

void kernel_taskFinished()
{
	g_task_running->m_state = Finished;
	SEGGER_SYSVIEW_OnTaskTerminate((unsigned)g_kernel_isRunning);
	kernel_yield(); //will never return as it is "blocks", but is on no block list
}

void kernel_startTask(Task_TCB* tcb, EntryPointFunc entryPoint, void* param1)
{
	seggerHelperCreateTask(tcb, tcb->m_stackPtr - (uint32_t*)(tcb->m_stackBase)); //Stack is unmodified, can calculate size
	//build its stack frame
	tcb->m_stackPtr -= 8; //make space for frame (8 words)
	// allign
	uint32_t stackPtrInt = (uint32_t)(tcb->m_stackPtr);
	stackPtrInt -= (stackPtrInt % 8); //8 byte allignment
	tcb->m_stackPtr = (uint32_t*) stackPtrInt;
	*(tcb->m_stackPtr + 0) = (uint32_t) param1;   //R 0 = 1st parameter
	*(tcb->m_stackPtr + 1) = 0xBBBBBBBB;          //R 1
	*(tcb->m_stackPtr + 2) = 0xBBBBBBBB;          //R 2
	*(tcb->m_stackPtr + 3) = 0xBBBBBBBB;          //R 3
	*(tcb->m_stackPtr + 4) = 0xBBBBBBBB;          //R12
	*(tcb->m_stackPtr + 5) = (uint32_t)kernel_taskFinished; // LR = return from user function will return here
	*(tcb->m_stackPtr + 6) = (int32_t)entryPoint; // PC
	*(tcb->m_stackPtr + 7) = 0x01000000;          //xPSR

	__disable_irq();
	taskList_pushFront(&g_tasks_ready, tcb);
	__enable_irq();
}

void kernel_yield()
{
	//SCB->ICSR |= SCB_ICSR_PENDSVSET_Msk;
	asm volatile ("SVC 0x33\n");
}

void kernel_start()
{
	kernel_startTask(&g_idleTask, idle_task_main, 0);
	g_kernel_isRunning = 1;
	kernel_yield();
}


void kernel_switchContextToTask(Task_TCB* nextTask)
{
	if(nextTask == g_task_running) return; //nothing to do
	if(g_task_running) //should always be true except on kernelStart
	{
		//switch from last Task
		g_task_running->m_stackPtr = (uint32_t*) __get_PSP();
		if(g_task_running->m_state == Running)
		{
			//the running task did not block -> still ready (otherwise it would already be on a blocked list)
			g_task_running->m_state = Ready;
			taskList_pushFront(&g_tasks_ready, g_task_running);
		}
	}
	//switch to nextTask
	__set_PSP((uint32_t)(nextTask->m_stackPtr));
	//assert(task_inList(readyList, nextTask))
	assert(nextTask->m_state == Ready);
	taskList_remove(&g_tasks_ready, nextTask);
	g_task_running = nextTask;
	g_task_running->m_state = Running;
}

#define RETURN_TO_THREAD_PSP() \
	asm volatile(\
		"MOV	LR, #0xFFFFFFFD\n"\
		"BX		LR\n"\
	)

#define RETURN_TO_HANDLER_MSP() \
	asm volatile(\
		"MOV	LR, #0xFFFFFFF9\n"\
		"BX		LR\n"\
	)

__attribute((naked))
void SV_Handler(void)
{
	//switched to main Stack
	__disable_irq(); //Sv shall not be interrupted (optimizable)
	SAVE_REMAINING_REGISTERS_OF_CURRENT_TASK("SvcHandler");

	if(g_task_running)
		SEGGER_SYSVIEW_OnTaskStopExec();

	kernel_updateTasks();
	Task_TCB* nextTask = kernel_getNextTask();
	kernel_switchContextToTask(nextTask);

	__set_CONTROL(__get_CONTROL() & ~0x04); //ensure no FP
	__set_CONTROL(__get_CONTROL() & ~0x01); //privileged threads

	SEGGER_SYSVIEW_OnTaskStartExec((unsigned)g_task_running);

	RESTORE_REMAINING_REGISTERS_OF_CURRENT_TASK("SvcHandler");
	__enable_irq();
	RETURN_TO_THREAD_PSP();
}
