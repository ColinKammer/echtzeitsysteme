#include "mutex.h"

#include <assert.h>

void mutex_lock(Mutex* mtx)
{
	semaphore_acquire(&(mtx->m_sem));
	mtx->m_lockOwner = g_task_running;
}

int mutex_tryLock(Mutex* mtx)
{
	if(semaphore_tryAcquire(&(mtx->m_sem)))
	{
		mtx->m_lockOwner = g_task_running;
		return 1;
	}
	else
	{
		return 0;
	}
}

void mutex_unlock(Mutex* mtx)
{
	assert(mtx->m_lockOwner == g_task_running);
	semaphore_release_one(&(mtx->m_sem));
}
