#pragma once

#include <stdint.h>
#include <stddef.h>

#include "task.h"

typedef void (*TimerTickHandler)(void);

typedef enum {
	TimerStateDisabled,
	TimerStateEnabled
} KernelTimerState;

typedef struct {
	Task_TCB* m_task;
	uint32_t m_interval;
	volatile KernelTimerState m_state;
	TimerTickHandler m_tickHandler;
} KernelTimer;


#define KERNEL_SOFTTIMER(NAME, TASK_STRNAME, PRIORITY, STACKSIZE, TASK_ID, TICKHANDLER)\
	KERNEL_CREATETASK(tmr_##NAME##_task, TASK_STRNAME, PRIORITY, STACKSIZE, TASK_ID)\
	KernelTimer NAME = {\
		.m_task = &tmr_##NAME##_task,\
		.m_interval = 0,\
		.m_state = TimerStateDisabled,\
		.m_tickHandler = TICKHANDLER\
	};

void kernel_timer_setInterval(KernelTimer* tmr, uint32_t intervalMs);

void kernel_timer_start(KernelTimer* tmr);
void kernel_timer_stop(KernelTimer* tmr);


