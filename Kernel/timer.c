#include "timer.h"

#include <assert.h>

#include "kernel.h"
#include "block_timed.h"

void timerMain(void* kernelTimer)
{
	KernelTimer* tmr = (KernelTimer*)kernelTimer;
	uint32_t nextExecTime = durationToEndTicks(tmr->m_interval);
	while(1)
	{
		kernel_task_block_time(tmr->m_task, nextExecTime);
		if(tmr->m_state != TimerStateEnabled) return;
		tmr->m_tickHandler();
		nextExecTime += durationToTicks(tmr->m_interval); //avoid integral Error, prefer jitter
		assert(nextExecTime > durationToEndTicks(0)); //should be in the future, maybe handler takes to long?
	}
}

void kernel_timer_setInterval(KernelTimer* tmr, uint32_t intervalMs)
{
	assert(intervalMs > 0);
	tmr->m_interval = intervalMs;
}

void kernel_timer_start(KernelTimer* tmr)
{
	assert(tmr->m_interval > 0);
	if(tmr->m_state == TimerStateEnabled) return; //already running

	tmr->m_state = TimerStateEnabled;
	kernel_startTask(tmr->m_task, timerMain, (void*)tmr);

}

void kernel_timer_stop(KernelTimer* tmr)
{
	tmr->m_state = TimerStateDisabled;
}
