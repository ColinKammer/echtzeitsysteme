#include "Segger/helper.h"

#include <string.h>
#include "SEGGER_SYSVIEW.h"

void seggerHelperCreateTask(const Task_TCB* osTask, uint32_t stackSize)
{
 SEGGER_SYSVIEW_TASKINFO segInfo;
 SEGGER_SYSVIEW_OnTaskCreate((unsigned)osTask);
 memset(&segInfo, 0, sizeof(segInfo));
 segInfo.TaskID = (U32)osTask;
 segInfo.sName = osTask->m_name;
 segInfo.Prio = osTask->m_priority;
 segInfo.StackBase = (U32)osTask->m_stackBase;
 segInfo.StackSize = stackSize;
 SEGGER_SYSVIEW_SendTaskInfo(&segInfo);
}
