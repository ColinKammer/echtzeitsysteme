#pragma once
#include "SEGGER_SYSVIEW.h"

#include <stdint.h>
#include "task.h"

extern void seggerHelperCreateTask(const Task_TCB* osTask, uint32_t stackSize);
