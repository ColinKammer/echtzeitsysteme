#include "block_timed.h"

#include <assert.h>

#include "kernel.h"
#include "taskList.h"
#include "main.h"
#include "Segger/helper.h"

Task_TCB* g_tasks_blocked_byTime = NULL;

void kernel_task_block_time(Task_TCB* tcb, uint32_t untilTicksEq)
{
	assert(tcb == g_task_running);
	__disable_irq();
	SEGGER_SYSVIEW_OnTaskStopReady((unsigned)tcb, 'T');
	uint32_t endTime = untilTicksEq;
	tcb->m_blockContext = &endTime;
	tcb->m_blockParam = NULL;
	tcb->m_state = BlockedByTime;
	taskList_pushFront(&g_tasks_blocked_byTime, tcb);
	__enable_irq();
	kernel_yield();
}

void kernel_task_block_time_updateAll()
{
	Task_TCB* ittr = g_tasks_blocked_byTime;
	while(ittr != NULL)
	{
		Task_TCB* cTask = ittr;
		ittr = ittr->m_nextInList;

		assert(cTask->m_state == BlockedByTime);
		uint32_t* endTime = cTask->m_blockContext;
		if(HAL_GetTick() >= (*endTime))
		{
			taskList_remove(&g_tasks_blocked_byTime, cTask);
			kernel_task_ready(cTask);
		}
	}
}

uint32_t durationToTicks(uint32_t durationMs)
{
	return durationMs / HAL_GetTickFreq();
}

uint32_t durationToEndTicks(uint32_t durationMs)
{
	return HAL_GetTick() + durationToTicks(durationMs);
}
