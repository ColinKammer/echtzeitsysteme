#include "taskList.h"

#include <stdlib.h>
#include <assert.h>

void taskList_pushFront(Task_TCB** list, Task_TCB* element)
{
	assert(element->m_nextInList == NULL);
	assert(element->m_prevInList == NULL);
	element->m_nextInList = *list;
	element->m_prevInList = NULL;
	(*list)->m_prevInList = element; //swing over backwards link
	*list = element;                 //swing over forwards link
}

void taskList_remove(Task_TCB** list, Task_TCB* element)
{
	if(element->m_nextInList != NULL)
	{
		//swing over backwards link
		element->m_nextInList->m_prevInList = element->m_prevInList;
	}
	if(element->m_prevInList != NULL)
	{
		//swing over forwards link (somewhere in the middle of the list)
		element->m_prevInList->m_nextInList = element->m_nextInList;
	}
	else
	{
		//swing over forwards link (head of the list)
		*list = element->m_nextInList;
	}
	element->m_nextInList = NULL;
	element->m_prevInList = NULL;
}
