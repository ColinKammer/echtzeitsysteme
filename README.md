# Features
## Yield
 * tasks can yield at any time to rerun the scheduler

## Semaphore
 * Counting semaphore
 * Supported Operations:
    * acquire
    * tryAcquire (good if there are alternatives for the ressource)
    * release_one
    * release_allWaiting (flush/broadcast)
 * Implemented using locking interrupts

## Mutex
 * NonRecursive
 * Supported Operations:
    * lock
    * tryLock (good if there are alternatives, e.g. failing to avoid deadlocks)
    * unlock
 * Implemented using Semaphore
 * Same task must unlock, that has locked

## Message Queue
 * Supported Operations:
    * trySend (with broadcast flag)
    * tryReceive (return with errorCode if empty)
    * receive (block till something is avilable)
 * Implementations:
    * Using Ringbuffer

## Block with custom checkFunktion that will be run in kernel space (customCbBlock)
<b> Great for polling </b>
1. Define a lambda-like callback funktion (funktionPointer + context)
2. Pass it to kernel_task_block_customCb
3. The task will block, your lamba will be called every time before the scheduler runs, when it returns true your task will unblock

## Sleep/BlockTimed
 * wait a given amount of time, without busyWaiting

## Timer
 * Supported Operations:
    * setIntervall
    * start
    * stop
 * Implemented using a separte task per timer and Sleep(blockTimed)

 # Tasks
  * static creation
  * start with single parameter (anonymized)
  * can terminate by returning

 # Scheduling
 * Priority based
 * Complexity:
    * linear: No of Ready Tasks
        * could be improved to 1, but then changing priority and becoming ready is no longer trivial
    * linear: No of customCbBlocked Tasks
        * just use this feature responsibly, don't poll everything
    * linear: No of Sleeping Tasks
        * could easily be improved to no of tasks waking up, by keeping the list ordered

# Tests
 * Basic, Crude
 * each feature is used, but not close to it's full extend